<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package unite
 */
?>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
			<div class="row">
				<nav role="navigation" class="col-md-6">
					<p>&copy; Le 37 | Centre de planning familial et de consultation familiale et conjugale<br>agréé et subventionné par la région wallonne.</p>
					<p>Rue Saint-Gilles, 29/03 -  4000 Liège  /  04 223 77 89  /  <a href="mailto:info@le37.be">info@le37.be</a></p>
				</nav>

				<div class="copyright col-md-6">

				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>