<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package unite
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header page-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->
		<div class="entry-content">
		<?php 
			if (is_page('13')){
		?>
				<div id="map">
						<?php echo do_shortcode( '[google_maps id="168"]' ); ?>
				</div>
				<div id="contact">
					<h2>Nos coordonnées</h2>
					<p id="adresse">Rue Saint Gilles 29/03<br/>
					4000 Liège</p>
					<p id="tel">Tél. : +32 (0)4/223.77.89 </p>
					<p id="email">E-mail : <a href="mailto:info@le37.be">info@le37.be</a> </p>
				</div>
		<?php
		}
		?>

		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'unite' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'unite' ), '<footer class="entry-meta"><i class="fa fa-pencil-square-o"></i><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->
