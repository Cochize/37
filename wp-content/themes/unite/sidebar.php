<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package unite
 */
?>
	<div id="secondary" class="widget-area col-sm-12 col-md-3" role="complementary">

		<?php do_action( 'before_sidebar' ); ?>
		
		<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>


			<aside id="meta" class="widget">
				<h1 class="widget-title"><?php _e( 'Meta', 'unite' ); ?></h1>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>
			</aside>

		<?php endif; // end sidebar widget area ?>

			<?php
			if (is_front_page()) {
				include('event_sidebar.php'); 
			} else { } ?>

		
	</div><!-- #secondary -->
